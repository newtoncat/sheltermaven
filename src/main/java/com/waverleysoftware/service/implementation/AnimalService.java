package com.waverleysoftware.service.implementation;

import com.waverleysoftware.model.Animal;
import com.waverleysoftware.model.Cat;
import com.waverleysoftware.model.Dog;
import com.waverleysoftware.model.Person;
import com.waverleysoftware.service.provider.IAnimalService;
import com.waverleysoftware.storage.AnimalStorage;
import one.util.streamex.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class AnimalService implements IAnimalService {

    private List<Animal> animals;

    public AnimalService() {
        animals = new AnimalStorage().getAnimals();
    }


    @Override
    public List<Animal> getAnimals() {
        return StreamEx.of(animals)
                .toList();
    }

    @Override
    public List<Animal> getAnimalByType(Class<? extends Animal> unknownAnimal) {
        return StreamEx.of(animals)
                .filter(animalInstance -> unknownAnimal.isInstance(animalInstance))
                .toList();
    }

    @Override
    public Animal getAnimalByID(UUID uuid) {
        List<Animal> filteredAnimals=  StreamEx.of(animals)
                .filter(animal -> animal.getId().equals(uuid))
                .toList();
        return filteredAnimals.isEmpty() ? null : filteredAnimals.get(0);
    }

    @Override
    public void deleteAnimal(UUID uuid) {
       animals = StreamEx.of(animals)
                .filter(animal -> !animal.getId().equals(uuid))
                .toList();
    }

}
