package com.waverleysoftware.facade;

import com.waverleysoftware.model.Animal;
import com.waverleysoftware.service.implementation.AnimalService;
import com.waverleysoftware.model.Person;
import com.waverleysoftware.model.Cat;
import com.waverleysoftware.model.Dog;

import java.util.UUID;

import java.util.List;

public class ShelterFacade {
    private final AnimalService animalService;


    public ShelterFacade(AnimalService animalService) {

        this.animalService = animalService;
    }

    public void pickUpAnimal(UUID uuid, String firstName, String lastName, int age) {
        Person person = new Person(firstName, lastName, age);
        animalService.deleteAnimal(uuid);
        System.out.println("You took an animal");
        person.toString();
    }

    public List<Animal> getAnimalByType(String type) {
        if (type.equals("Cat")) {
           return animalService.getAnimalByType(Cat.class);
        }
        else if (type.equals("Dog")){
           return animalService.getAnimalByType(Dog.class);
        } else {
            System.out.println("Unknown animal");
            return null;
        }
    }
}
