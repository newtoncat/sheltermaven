package com.waverleysoftware.model;

public enum HomeType {

    OUTDOOR,
    INDOOR

}