package com.waverleysoftware;

import com.waverleysoftware.facade.ShelterFacade;
import com.waverleysoftware.model.Cat;
import com.waverleysoftware.service.implementation.AnimalService;

import java.util.Scanner;
import java.util.UUID;

import static io.vavr.API.*;
import static org.apache.commons.lang3.math.NumberUtils.toInt;


class Main {

    private static void dogPicture(){
        System.out.println("\n" +
                "     /^-----^\\\n" +
                "     V  o o  V\n" +
                "      |  Y  |\n" +
                "       \\ Q /\n" +
                "       / - \\\n" +
                "       |    \\\n" +
                "       |     \\    )\n" +
                "       || (___\\");
    }

    private static void mainMenu() {
        System.out.println("\nWELCOME TO SHELTER!!!!!\n" +
                "Please, select an option:\n" +
                "1. Display all available animals\n" +
                "2. Filter animals by type\n" +
                "3. Choose animal by id\n" +
                "4. Pick up animal\n" +
                "5. Exit.\n");
    }

    public static void main(String[] args) {

        AnimalService animalService = new AnimalService();
        ShelterFacade facade = new ShelterFacade(animalService);
        dogPicture();

        try (Scanner console = new Scanner(System.in)) {
            while (true) {
                mainMenu();

                Match(toInt(console.nextLine())).of(
                        Case($(1), () -> run(() -> {
                            System.out.println(animalService.getAnimals());
                        })),

                        Case($(2), () -> run(() -> {
                            System.out.println("Cat or dog ?");
                            System.out.println(facade.getAnimalByType(console.nextLine()));
                        })),

                        Case($(3), () -> run(() -> {
                            UUID uuid = null;

                            try{
                                uuid  = UUID.fromString(console.nextLine());
                            }
                            catch(IllegalArgumentException e) {
                                System.out.println("Invalid uuid");
                                return;
                            }
                            System.out.println(animalService.getAnimalByID(uuid));
                        })),

                        Case($(4), () -> run(() -> {
                            UUID uuid = null;

                            try{
                                uuid  = UUID.fromString(console.nextLine());
                            }
                            catch(IllegalArgumentException e) {
                                System.out.println("Invalid uuid");
                                return;
                            }
                            System.out.println("Please enter your name: ");
                            String firstName = console.nextLine();
                            System.out.println("Please enter your surname: ");
                            String lastName = console.nextLine();
                            System.out.println("Please enter your age: ");
                            int age = toInt(console.nextLine());

                            facade.pickUpAnimal(uuid, firstName, lastName, age);
                        })),


                        Case($(5), () -> run(() -> System.exit(0)))

                );
            }



        }
    }
}